import {ModuleMetadata} from "@nestjs/common/interfaces";

export function moduleMetadataFrom(srcModule): ModuleMetadata {
  const metadata = Reflect.getMetadataKeys(srcModule)
    .filter(key => key !== "modules" || key !== "components")
    .reduce((agg, key) => ({...agg, [key]: Reflect.getMetadata(key, srcModule)}), {});
  return metadata;
}
