export function flatMap<T, R>(this: T[], projection: (item: T, index: number) => R[]): R[] {
  return [].concat(...this.map((x, index) => projection(x, index)));
}

export function isEmpty<T>(this: T[]): boolean {
  return this && !this.length;
}

export function toObjectMap<T>(this: T[], idSelector: (i: T) => string): {[key: string]: T} {
  return this.reduce((agg, next) => ({...agg, [idSelector(next)]: next}), {});
}

Array.prototype.flatMap = flatMap;
Array.prototype.isEmpty = isEmpty;
Array.prototype.toObjectMap = toObjectMap;

declare global {
  interface Array<T> {
    flatMap: typeof flatMap;
    isEmpty: typeof isEmpty;
    toObjectMap: typeof toObjectMap;
  }
}
