import {ApiModelProperty} from "@nestjs/swagger";

import * as moment from "moment";

import {AirportDef} from "./airport.dto";
import {Company} from "./company.dto";

export class FlightAirport extends AirportDef {
  @ApiModelProperty({type: Date})
  public readonly date: Date;

  constructor(def: AirportDef, date: Date) {
    super(def.identifier, def.name, def.country);
    this.date = date;
  }
}

export class Cost {
  @ApiModelProperty({type: Number})
  public readonly price: number;

  constructor(price: number) {
    this.price = price;
  }
}

export class Flight extends Cost {
  @ApiModelProperty({type: String})
  public readonly companyName: string;

  @ApiModelProperty({type: FlightAirport})
  public readonly departure: FlightAirport;

  @ApiModelProperty({type: FlightAirport})
  public readonly arrives: FlightAirport;

  constructor(departure: FlightAirport, arrives: FlightAirport, company: Company, price: number) {
    super(price);
    this.departure = departure;
    this.arrives = arrives;
    this.companyName = company.name;
  }

  public destinationMatch(iataCode: string): boolean {
    return this.arrives.identifier === iataCode;
  }
}

export class RoundTripFlight extends Cost {
  @ApiModelProperty({type: Flight})
  public readonly destination: Flight;

  @ApiModelProperty({type: Flight})
  public readonly back: Flight;

  constructor(destination: Flight, back: Flight, totalPrice: number) {
    super(totalPrice);
    this.destination = destination;
    this.back = back;
  }

  public days(): number {
    return moment(this.back.arrives.date).diff(moment(this.destination.arrives.date), "days");
  }
}
