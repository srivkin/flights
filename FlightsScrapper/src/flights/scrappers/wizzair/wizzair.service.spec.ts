import {Test, TestingModule} from "@nestjs/testing";
import {moduleMetadataFrom} from "../../../core";

import {WizzairModule, WizzairService} from "./wizzair.module";

describe("WizzairService", () => {
  let service: WizzairService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule(
      moduleMetadataFrom(WizzairModule)
    ).compile();
    service = module.get<WizzairService>(WizzairService);
  });
  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
