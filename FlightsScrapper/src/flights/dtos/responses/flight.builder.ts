import {Company} from "./company.dto";
import {AirportDef} from "./airport.dto";
import {Flight, FlightAirport} from "./flight.dto";

interface IAirportDescriptionBuilder {
  description(name: string, countryName: string): AirportDefBuilder;
}

export class AirportDefBuilder implements IAirportDescriptionBuilder {
  public static withCode(iataCode: string): IAirportDescriptionBuilder {
    return new AirportDefBuilder(iataCode);
  }

  private _name: string;
  private _countryName: string;
  private constructor(private _iataCode: string) {}

  public description(name: string, countryName: string): AirportDefBuilder {
    this._name = name;
    this._countryName = countryName;
    return this;
  }

  public build(): AirportDef {
    return new AirportDef(this._iataCode, this._name, this._countryName);
  }
}

interface IFlightBuilder {
  build(): Flight;
}

interface IFlightDestDateBuilder {
  arrives(date: Date): IFlightBuilder;
}

interface IFlightDstBuilder {
  destination(builder: AirportDefBuilder): IFlightDestDateBuilder;
}

interface IFlightSrcDateBuilder {
  departure(date: Date): IFlightDstBuilder;
}

interface IFlightSrcBuilder {
  source(builder: AirportDefBuilder): IFlightSrcDateBuilder;
}

interface IFlightCostBuilder {
  flightWillCost(value: number): IFlightSrcBuilder;
}

export class FlightBuilder implements IFlightCostBuilder, IFlightBuilder {
  public static for(companyName: string): IFlightCostBuilder {
    return new FlightBuilder(new Company(companyName));
  }

  private _value: number;
  private _sourceFlight: FlightAirport;
  private _destinationFlight: FlightAirport;

  constructor(private _company: Company) {}

  public flightWillCost(value: number): IFlightSrcBuilder {
    this._value = value;
    return new FlightBuilder.SourceFlightAirportBuilder(this);
  }

  public build(): Flight {
    return new Flight(this._sourceFlight, this._destinationFlight, this._company, this._value);
  }

  private static SourceFlightAirportBuilder = class
    implements IFlightSrcBuilder, IFlightSrcDateBuilder {
    private _flightBuilder: AirportDefBuilder;

    constructor(private _parent: FlightBuilder) {}

    public source(builder: AirportDefBuilder): IFlightSrcDateBuilder {
      this._flightBuilder = builder;
      return this;
    }

    public departure(date: Date): IFlightDstBuilder {
      this._parent._sourceFlight = new FlightAirport(this._flightBuilder.build(), date);
      const parent = this._parent;
      this._parent = null;
      return new FlightBuilder.DestinationFlightAirportBuilder(parent);
    }
  };

  private static DestinationFlightAirportBuilder = class
    implements IFlightDstBuilder, IFlightDestDateBuilder {
    private _flightBuilder: AirportDefBuilder;

    constructor(private _parent: FlightBuilder) {}

    public destination(builder: AirportDefBuilder): IFlightDestDateBuilder {
      this._flightBuilder = builder;
      return this;
    }

    public arrives(date: Date): IFlightBuilder {
      this._parent._destinationFlight = new FlightAirport(this._flightBuilder.build(), date);
      const parent = this._parent;
      this._parent = null;

      return parent;
    }
  };
}
