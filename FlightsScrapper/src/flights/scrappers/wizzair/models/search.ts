export interface IWizzairFareChartSearchRequest {
  wdc: false;
  flightList: FlightListItem[];
  dayInterval: number;
  adultCount: number;
  childCount: number;
  isRescueFare: boolean;
}

export interface FlightListItem {
  departureStation: string;
  arrivalStation: string;
  date: string;
}
