import * as moment from "moment";

const sliceTheDate = (m: moment.Moment) =>
  m
    .hours(0)
    .minutes(0)
    .seconds(0);

export const DATE_FORMAT = "DD-MM-YYYY";

export const TODAY = sliceTheDate(moment(Date.now()));

export const momentToClassValidatorFix = (m: moment.Moment) => m.add(1, "d"); // a bug, date transformation causing to subtract 1 date

export function toDate(): (v: any) => Date {
  return (v: any) => {
    const numericDate = moment(parseInt(v, 10));
    const textDate = moment(v, DATE_FORMAT);

    const resultDate = (textDate.isValid() && momentToClassValidatorFix(textDate)) || numericDate;
    return sliceTheDate(resultDate).toDate();
  };
}
