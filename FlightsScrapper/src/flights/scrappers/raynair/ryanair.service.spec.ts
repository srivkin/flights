import {Test, TestingModule} from "@nestjs/testing";
import {moduleMetadataFrom} from "../../../core";

import {RyanairModule, RyanairService} from "./ryanair.module";

describe("WizzairService", () => {
  let service: RyanairService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule(
      moduleMetadataFrom(RyanairModule)
    ).compile();
    service = module.get<RyanairService>(RyanairService);
  });
  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
