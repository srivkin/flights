import {bootstrap as mainBootstrap} from "./main";

declare const module: any;

async function bootstrap() {
  const app = await mainBootstrap();

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
