export interface IWizzairConnection {
  iata: string;
  operationStartDate: Date;
  rescueEndDate: Date;
  isDomestic: boolean;
}

export interface IWizzairAirportsPayload {
  cities: IWizzairAirport[];
}

export interface IWizzairAirport {
  iata: string;
  longitude: number;
  currencyCode: string;
  latitude: number;
  shortName: string;
  countryName: string;
  countryCode: string;
  connections: IWizzairConnection[];
  aliases: string[];
  isExcludedFromGeoLocation: boolean;
  rank: number;
  categories: number[];
}
