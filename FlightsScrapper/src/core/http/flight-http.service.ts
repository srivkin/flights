import {Injectable, Logger} from "@nestjs/common";
import * as rp from "request-promise";
import {Cookie} from "request-cookies";
import {EMPTY, Observable} from "rxjs";
import {from} from "rxjs/index";
import {catchError} from "rxjs/operators";

@Injectable()
export class FlightHttpService {
    private baseUrl: string;
    private cookie: string;

    constructor() {
    }

    public setBaseUrl(baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    public setCooke(cookie: string) {
        this.cookie = cookie;
    }

    public get(url: string): Observable<any> {
        const fullUrl = this.getFullUrl(url);

        Logger.log(`GET url: ${fullUrl}`);

        const options = {
            json: true,
            headers: {
                'Postman-Token': "98b7fd17-53c4-485a-bc89-c1d65b304093",
                'User-Agent': "PostmanRuntime/7.6.0",
                'accept': 'application/json',
                'accept-language': 'en-US,en;q=0.9,ar;q=0.8',
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'origin': 'https://be.wizzair.com',
                'cooke': this.cookie,
                'pragma': 'no-cache'
            }
        };

        return from(rp.get(fullUrl, options));
    }

    public setCookeFromUrl(url: string, cookieKey: string): Observable<any> {
        Logger.log(`GET Cooke url: ${this.getFullUrl(url)}`);


        const options = {
            method: 'get',
            url: this.getFullUrl(url),
            followRedirect: false,
            resolveWithFullResponse: true,
            headers: {
                'cache-control': "no-cache",
                'User-Agent': "PostmanRuntime/7.6.0",
                'Accept': "*/*",
                'Host': "be.wizzair.com",
                'accept-encoding': "null"
            }
        };

        // Todo major cleanup
        return from(rp(options))
            .pipe(
                catchError(err => {
                    const rawCookies: string[] = err.response.headers['set-cookie'];
                    if (rawCookies && rawCookies.length) {
                        const cookieStr = rawCookies.find(c => c.startsWith(cookieKey));
                        const cookie = new Cookie(cookieStr);
                        this.setCooke(cookie.key + '=' + cookie.value);
                    }
                    return EMPTY;
                })
            );
    }

    public post(url: string, payload: any): Observable<any> {
        const options = {
            body: payload, json: true, headers: {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                'referer': `${this.baseUrl}/${url}`,
                'accept': 'application/json',
                'accept-language': 'en-US,en;q=0.9,ar;q=0.8',
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'origin': 'https://be.wizzair.com',
                'cookie': this.cookie,
                'pragma': 'no-cache'
            }
        };

        return from(rp.post(this.getFullUrl(url), options));
    }

    //
    // public put(url: string, payload: any): Observable<any> {
    //     const options = {
    //         method: "PUT",
    //         uri: `${this.baseUrl}${url}`,
    //         body: payload,
    //         json: true // Automatically stringifies the body to JSON
    //     };
    //     return from(rp(options));
    // }
    //
    // public delete(url: string, id: string): Observable<any> {
    //     const options = {
    //         method: "DELETE",
    //         uri: `${this.baseUrl}${url}/${id}`,
    //         resolveWithFullResponse: true    //  <---  <---  <---  <---
    //     };
    //     return from(rp(options));
    // }

    private getFullUrl(url: string): string {
        return (url.startsWith('http') || !this.baseUrl) ? url : `${this.baseUrl}/${url}`;
    }
}
