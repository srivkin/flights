import {HttpModule, Module} from "@nestjs/common";

import {HTTP} from "../http.config";
import {RyanairService} from "./ryanair.service";
export {RyanairService} from "./ryanair.service";

@Module({
  imports: [
    HttpModule.register({
      ...HTTP,
      baseURL: "https://api.ryanair.com"
    })
  ],
  providers: [RyanairService],
  exports: [RyanairService]
})
export class RyanairModule {}
