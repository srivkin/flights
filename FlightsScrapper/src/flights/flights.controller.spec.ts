import {Test, TestingModule} from "@nestjs/testing";

import {moduleMetadataFrom} from "../core";

import {FlightsController} from "./flights.controller";
import {FlightsModule} from "./flights.module";

describe("Flights Controller", () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule(moduleMetadataFrom(FlightsModule)).compile();
  });
  it("should be defined", () => {
    const controller: FlightsController = module.get<FlightsController>(FlightsController);
    expect(controller).toBeDefined();
  });
});
