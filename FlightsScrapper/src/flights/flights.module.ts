import {Module} from "@nestjs/common";
import {FlightsController} from "./flights.controller";
import {FlightsService} from "./flights.service";
import {ScrapperApi, SCRAPPERS} from "./scrappers";

@Module({
  imports: [...SCRAPPERS.modules],
  controllers: [FlightsController],
  providers: [
    {
      provide: FlightsService,
      useFactory: (...scrapper: ScrapperApi[]) => new FlightsService(scrapper),
      inject: [...SCRAPPERS.services]
    }
  ]
})
export class FlightsModule {}
