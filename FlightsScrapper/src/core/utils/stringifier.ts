import {isArray} from "util";

export function stringifier(arrayLimit = 5) {
  return obj => {
    if (typeof obj === "string") {
      return obj; // We want to keep any formatting (if any)
    }

    let cache = [];
    const json = JSON.stringify(
      obj,
      (_, value) => {
        if (typeof value === "object" && value !== null) {
          if (cache.indexOf(value) !== -1) {
            // Circular reference found, discard key
            return;
          }
          // Store value in our collection
          cache.push(value);
        }

        if (isArray(value)) {
          return value.length > arrayLimit ? [...value.slice(0, arrayLimit), "..."] : value;
        }
        return value;
      },
      2
    );

    cache = null; // Enable garbage collection
    return json;
  };
}
