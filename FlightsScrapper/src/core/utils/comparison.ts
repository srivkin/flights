export type Comperator<A> = (a: A, b: A) => boolean;
export type Comparison<A> = (a: A, b: A) => number;
export type Compare = <A>(test: Comperator<A>) => Comparison<A>;

export const compare: Compare = test => (a, b) => (test(a, b) ? 1 : test(b, a) ? -1 : 0);

export const ascendingNumeric = compare((l: number, r: number) => l > r);
export const ascendingString = compare((l: string, r: string) => l.toLowerCase() > r.toLowerCase());
export const descendingNumeric = compare((l: number, r: number) => l < r);
export const descendingString = compare(
  (l: string, r: string) => l.toLowerCase() < r.toLowerCase()
);
