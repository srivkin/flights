import "./extensions/arrays";

import {NestFactory} from "@nestjs/core";
import {Logger, ValidationPipe} from "@nestjs/common";
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";

import {AppModule} from "./app.module";
import {RequestInterceptor} from "./core/logger/request.interceptor";

export async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalInterceptors(new RequestInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      skipMissingProperties: false,
      whitelist: true,
      forbidNonWhitelisted: true
    })
  );
  SwaggerModule.setup(
    "swagger",
    app,
    SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle(require("../package.json").name)
        .setDescription("Flight scrapper")
        .setVersion(`${process.env.API_VERSION}.1.2`)
        .build()
    )
  );

  await app
    .listen(process.env.PORT)
    .then(() =>
      Logger.log(`HTTP server successfully listing on port:'${process.env.PORT}'`, bootstrap.name)
    );

  return app;
}
bootstrap();
