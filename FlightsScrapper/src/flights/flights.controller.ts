import {Controller, Get, Query} from "@nestjs/common";
import {ApiOkResponse, ApiOperation} from "@nestjs/swagger";

import {OneWaySearchDto, TwoWaySearchDto} from "./dtos/requests";
import {CompanyAirports, Flight, RoundTripFlight} from "./dtos/responses";
import {FlightsService} from "./flights.service";
import {Route} from "./dtos/responses/route.dto";

@Controller("flights")
export class FlightsController {
  constructor(private _flights: FlightsService) {}

  @Get("routes")
  @ApiOperation({title: "Get all available routs"})
  @ApiOkResponse({type: Route, isArray: true})
  public routes() {
    return this._flights.routes();
  }

  @Get("airports")
  @ApiOperation({title: "Get all available airports"})
  @ApiOkResponse({type: CompanyAirports, isArray: true})
  public airports() {
    return this._flights.airports();
  }

  @Get("oneWay")
  @ApiOperation({title: "Get one way flights"})
  @ApiOkResponse({type: Flight, isArray: true})
  public oneWay(@Query() payload: OneWaySearchDto) {
    return this._flights.searchOneWay(payload);
  }

  @Get("roundTrip")
  @ApiOperation({title: "Get round trip flights"})
  @ApiOkResponse({type: RoundTripFlight, isArray: true})
  public roundTrip(@Query() payload: TwoWaySearchDto) {
    return this._flights.searchTwoWay(payload);
  }
}
