import {Injectable} from "@nestjs/common";

import * as moment from "moment";

import {combineLatest, Observable, range} from "rxjs";
import {map, reduce, flatMap, concatMap, mergeMap, distinct, filter} from "rxjs/operators";

import {ascendingNumeric, ascendingString, LogInputArgs} from "../core";

import {ScrapperApi} from "./scrappers";
import {
    AirportLocation,
    CompanyAirports,
    Flight,
    Company,
    RoundTripFlight,
    Cost
} from "./dtos/responses";
import {OneWaySearchDto, TwoWaySearchDto, TimeUnit} from "./dtos/requests";

const byCounterCodeAsc = (l: AirportLocation, r: AirportLocation) =>
    ascendingString(l.name, r.name);

const sortByPrice = <T extends Cost>(items: T[]): T[] =>
    items.sort((l: Cost, r: Cost) => ascendingNumeric(l.price, r.price));

@Injectable()
export class FlightsService {
    constructor(private _scrappers: ScrapperApi[]) {
    }

    public airports(): Observable<CompanyAirports[]> {
        const toCompany = (companyName: string) => (airports: AirportLocation[]) =>
            new CompanyAirports(new Company(companyName), airports.sort(byCounterCodeAsc));

        return combineLatest(
            this._scrappers.map(scrapper => scrapper.airports().pipe(map(toCompany(scrapper.company))))
        );
    }

    public routes(): Observable<any[]> {
        return combineLatest(this._scrappers.map(scrapper => scrapper.routes()));
    }

    @LogInputArgs()
    public searchOneWay(request: OneWaySearchDto): Observable<Flight[]> {
        return this.searchWith(s => s.oneWay(request));
    }

    @LogInputArgs()
    public searchTwoWay(request: TwoWaySearchDto): Observable<RoundTripFlight[]> {
        let lookup = (s: ScrapperApi) => s.roundTrip(request);
        if (request.predict) {
            lookup = this.predictOneWays(request);
        }

        return this.searchWith(lookup);
    }

    private searchWith<T extends Cost>(lookUp: (s: ScrapperApi) => Observable<T[]>): Observable<T[]> {

        return combineLatest(this._scrappers.map(scrapper => lookUp(scrapper)))
            .pipe(
                flatMap(f => f),
                reduce((agg, next) => agg.concat(next), []),
                map(sortByPrice)
            );
    }

    private predictOneWays(request: TwoWaySearchDto) {
        const searchDtoFactory = (dst: Flight) => (delta: number) => {
            const dstOneWaySearch = new OneWaySearchDto();

            dstOneWaySearch.fromAirport = dst.arrives.identifier;
            dstOneWaySearch.departureDate = moment(dst.arrives.date)
                .add(request.returnIn + delta, TimeUnit.Day)
                .toDate();
            dstOneWaySearch.forecast = TimeUnit.Week;

            return dstOneWaySearch;
        };

        const backToOrigin = (f: Flight) => f.destinationMatch(request.fromAirport);
        const toRoundTrip = (dst: Flight) => (back: Flight) =>
            new RoundTripFlight(dst, back, dst.price + back.price);

        return (scrapper: ScrapperApi) =>
            scrapper.oneWay(request).pipe(
                flatMap(destinations => destinations),
                concatMap(dst => {
                    const createSearchDtoUsing = searchDtoFactory(dst);
                    return range(0, request.scanRange).pipe(
                        mergeMap(delta =>
                            scrapper
                                .oneWay(createSearchDtoUsing(delta))
                                .pipe(flatMap(results => results.filter(backToOrigin).map(toRoundTrip(dst))))
                        ),
                        distinct(k => moment(k.back.departure.date).format("llll"))
                    );
                }),
                filter(rt => rt.days() <= request.limitDays),
                reduce<RoundTripFlight>((agg, next) => agg.concat(next), [])
            );
    }
}
