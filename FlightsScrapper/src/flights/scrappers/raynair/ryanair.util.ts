import {stringify as queryStringify} from "querystring";

import * as moment from "moment";

import {AirportDefBuilder, FlightBuilder, Flight, RoundTripFlight} from "../../dtos/responses";
import {OneWaySearchDto, TwoWaySearchDto} from "../../dtos/requests";

import {IRyanairFlight, IRyanairRoundTripFare} from "./models";
import {CURRENCY} from "../../../core";

export class RyanairUtil {
  public static readonly RA_NAME = "Raynair";
  private static readonly RA_DATE_FORMAT = "YYYY-MM-DD";

  public oneWayQuery(request: OneWaySearchDto): string {
    return queryStringify(this.composeOneWayQuery(request));
  }

  public roundTripQuery(request: TwoWaySearchDto): string {
    const back = moment(request.departureDate).add(request.returnIn, request.returnInTimeUnit);
    const twoWay = {
      inboundDepartureDateFrom: back.format(RyanairUtil.RA_DATE_FORMAT),
      inboundDepartureDateTo: back.format(RyanairUtil.RA_DATE_FORMAT)
    };
    return queryStringify({...this.composeOneWayQuery(request), ...twoWay});
  }

  public toRoundTrip(f: IRyanairRoundTripFare): RoundTripFlight {
    return new RoundTripFlight(
      this.toFlight(f.outbound),
      this.toFlight(f.inbound),
      f.summary.price.value
    );
  }

  public toFlight(f: IRyanairFlight): Flight {
    return FlightBuilder.for(RyanairUtil.RA_NAME)
      .flightWillCost(f.price.value)
      .source(
        AirportDefBuilder.withCode(f.departureAirport.iataCode).description(
          f.departureAirport.name,
          f.departureAirport.countryName
        )
      )
      .departure(new Date(f.departureDate))
      .destination(
        AirportDefBuilder.withCode(f.arrivalAirport.iataCode).description(
          f.arrivalAirport.name,
          f.arrivalAirport.countryName
        )
      )
      .arrives(new Date(f.arrivalDate))
      .build();
  }

  private composeOneWayQuery(request: OneWaySearchDto): {} {
    const md = moment(request.departureDate);
    const oneWay = {
      departureAirportIataCode: request.fromAirport,
      outboundDepartureDateFrom: md.format(RyanairUtil.RA_DATE_FORMAT),
      outboundDepartureDateTo: md.add(1, request.forecast).format(RyanairUtil.RA_DATE_FORMAT),
      currency: CURRENCY
    };
    return oneWay;
  }
}
