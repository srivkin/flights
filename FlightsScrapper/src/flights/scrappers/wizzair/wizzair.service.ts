import {Injectable} from "@nestjs/common";
import {combineLatest, from, Observable} from "rxjs";
import {flatMap, map, reduce, switchMap} from "rxjs/operators";
import {AirportLocation, Coordinate, Flight, RoundTripFlight} from "../../dtos/responses";
import {OneWaySearchDto, TwoWaySearchDto} from "../../dtos/requests";

import {ScrapperApi} from "../scrapper.api";

import {IWizzairRoundTripFare} from "./models";
import {WizzairUtil} from "./wizzair.util";
import {Route} from "../../dtos/responses/route.dto";
import {IWizzairConnection, IWizzairRoutesResult} from "./models/route";
import {AirportDef} from "../../dtos/responses";
import {IWizzairFareChartSearchRequest} from "./models/search";
import * as rp from "request-promise";
import {IWizzairOneWayFare} from "./models";
import {WizzairRoundTripRequest} from "./models/roundRequest";
import {TimeUnit} from "../../dtos/requests";
import moment = require("moment");
import {FlightHttpService} from "../../../core/http/flight-http.service";

@Injectable()
export class WizzairService implements ScrapperApi {
    private _util = new WizzairUtil();

    get company() {
        return WizzairUtil.RA_NAME;
    }

    constructor(private httpService: FlightHttpService) {
        this.getBaseUrl()
            .pipe(
                switchMap(res => {
                    this.httpService.setBaseUrl(res.apiUrl);
                    return this.getCookie();
                })
            ).subscribe();
    }

    public airports(): Observable<AirportLocation[]> {
        return this._routes().pipe(
            map(({cities}) =>
                cities.map(
                    airport =>
                        new AirportLocation(
                            airport.iata,
                            airport.shortName,
                            Coordinate.from(airport.longitude, airport.latitude)
                        )
                )
            )
        );
    }

    public getBaseUrl() {
        return this.httpService.get(`https://wizzair.com/static/metadata.json`);
    }

    public getCookie() {
        return this.httpService.setCookeFromUrl('auth/cookie', "ASP.NET_SessionId");
    }

    public routes(src?: string): Observable<Route[]> {
        return this._routes().pipe(
            map(({cities}) =>
                cities
                    .map(
                        airport =>
                            new Route(
                                airport.iata,
                                airport.shortName,
                                Coordinate.from(airport.longitude, airport.latitude),
                                airport.connections.map(
                                    (connection: IWizzairConnection) => new AirportDef(connection.iata, null, null)
                                )
                            )
                    )
                    .filter(route => {
                        return src ? route.identifier === src : true;
                    })
            )
        );
    }

    private _routes(): Observable<IWizzairRoutesResult> {
        return this.httpService.get(`asset/map?languageCode=en-gb`);
    }

    public oneWay(request: OneWaySearchDto): Observable<Flight[]> {
        return this.routes(request.fromAirport).pipe(
            map((routes: Route[]) => routes[0]),
            switchMap((route: Route) => {
                return combineLatest(
                    route.connections.map(connection =>
                        this._oneWay(request.fromAirport, connection.identifier, request.departureDate)
                            .pipe(
                                map(f => f.outboundFlights.map(fl =>
                                    this._util.toFlight(fl)).filter(fli => fli.price)
                                )
                            )
                    )
                );
            }),
            flatMap(d => d)
        );
    }

    private _oneWay(src: string, dest: string, date: Date): Observable<IWizzairOneWayFare> {
        const url = `asset/farechart`;
        const md = moment(date);
        const payload: IWizzairFareChartSearchRequest = {
            wdc: false,
            flightList: [
                {
                    departureStation: src.toUpperCase(),
                    arrivalStation: dest.toUpperCase(),
                    date: md.format("YYYY-MM-DD").toString()
                }
            ],
            dayInterval: 3,
            adultCount: 1,
            childCount: 0,
            isRescueFare: false
        };

        return this.httpService.post(url, payload);
    }

    private _roundTrip(src: string, dest: string, departureDate: Date, returnIn: number, forecast: TimeUnit)
        : Observable<IWizzairRoundTripFare> {
        const url = `search/timetable`;
        const payload: WizzairRoundTripRequest = {
            flightList: [
                {
                    departureStation: src,
                    arrivalStation: dest,
                    from: moment(departureDate).format("YYYY-MM-DD").toString(),
                    to: moment(departureDate).add(1, "month").format("YYYY-MM-DD").toString()
                },
                {
                    departureStation: dest,
                    arrivalStation: src,
                    from: moment(departureDate).add(returnIn, forecast).format("YYYY-MM-DD").toString(),
                    to: moment(departureDate).add(returnIn, forecast).add(1, "month").format("YYYY-MM-DD").toString(),
                }
            ],
            priceType: "regular",
            adultCount: 1,
            childCount: 0,
            infantCount: 0
        };
        const options = {
            method: "POST",
            uri: url,
            body: payload,
            json: true // Automatically stringifies the body to JSON
        };

        return from(rp<IWizzairRoundTripFare>(options));
    }

    public roundTrip(request: TwoWaySearchDto): Observable<RoundTripFlight[]> {
        return this.routes(request.fromAirport)
            .pipe(
                map((routes: Route[]) => routes[0]),
                switchMap((route: Route) =>
                    combineLatest(
                        route.connections.map(connection => {
                                return this._roundTrip(request.fromAirport, connection.identifier, request.departureDate, request.returnIn, request.forecast);
                            }
                        )
                    )),
                map(owl => owl
                    .filter(rt => !!rt.outboundFlights && !!rt.returnFlights)
                    .map(rt => this._util.toRoundTrip(rt))),
                flatMap(d => d),
                reduce((agg, next) => agg.concat(next), []),
                map(l => l.sort((a, b) => a.price - b.price))
            );
    }
}
