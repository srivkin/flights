export interface IWizzairRoute {
  iata: string;
  longitude: number;
  currencyCode: string;
  latitude: number;
  shortName: string;
  countryName: string;
  countryCode: string;
  connections: IWizzairConnection[];
}

export interface IWizzairConnection {
  iata: string;
  operationStartDate: Date;
  rescueEndDate: Date;
  isDomestic: boolean;
}

export interface IWizzairRoutesResult {
  cities: IWizzairRoute[];
}
