import {Type} from "@nestjs/common";

import {RyanairModule, RyanairService} from "./raynair/ryanair.module";
import {ScrapperApi} from "./scrapper.api";
import {WizzairModule, WizzairService} from "./wizzair/wizzair.module";

export {ScrapperApi} from "./scrapper.api";

export const SCRAPPERS = {
  modules: [WizzairModule, RyanairModule] as Type<any>[],
  services: ([WizzairService, RyanairService] as any) as ScrapperApi[]
};
