import {NestInterceptor, ExecutionContext, Injectable, Logger} from "@nestjs/common";

import {Observable} from "rxjs";
import {Request} from "express";

import {stringifier, isEmptyObject} from "../utils";

@Injectable()
export class RequestInterceptor implements NestInterceptor<any, any> {
  public intercept(
    context: ExecutionContext,
    call$: Observable<any>
  ): Observable<any> | Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest<Request>();
    const {baseUrl, path, method, query} = request;

    const queryParams = !isEmptyObject(query) ? `?query:${stringifier()(query)}` : "";

    const requestPath = `${baseUrl}${path}`;
    const webApiHandler = `${context.getClass().name}/${context.getHandler().name}`;

    Logger.log(
      `{${requestPath}${queryParams}, ${method}} -> ${webApiHandler}`,
      RequestInterceptor.name,
      false
    );

    if (process.env.LOGGER_LEVEL === "debug") {
      const {headers, body} = request;
      if (headers["content-type"] === "application/json") {
        Logger.log(`Body: ${stringifier()(body)}`, RequestInterceptor.name, false);
      }
    }

    return call$;
  }
}
