import {ApiModelProperty} from "@nestjs/swagger";

export class Coordinate {
  public static from(longitude: number, latitude: number): Coordinate {
    return new Coordinate(latitude, longitude);
  }

  @ApiModelProperty({type: Number})
  public readonly latitude: number;
  @ApiModelProperty({type: Number})
  public readonly longitude: number;

  private constructor(longitude: number, latitude: number) {
    this.longitude = longitude;
    this.latitude = latitude;
  }
}

export class AirportDef {
  @ApiModelProperty({type: String})
  public readonly identifier: string;

  @ApiModelProperty({type: String})
  public readonly name: string;

  @ApiModelProperty({type: String, required: false})
  public readonly country?: string;

  constructor(identifier: string, name: string, country: string) {
    this.identifier = identifier;
    this.name = name;
    this.country = country;
  }
}

export class AirportLocation extends AirportDef {
  @ApiModelProperty({type: Coordinate})
  public readonly coordinate: Coordinate;

  constructor(identifier: string, name: string, coordinate: Coordinate) {
    super(identifier, name, undefined);
    this.coordinate = coordinate;
  }
}
