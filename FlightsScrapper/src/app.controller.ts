import {Get, Controller} from "@nestjs/common";

@Controller()
export class AppController {
  constructor() {}

  @Get()
  public root(): any {
    const {name, version} = require("../package.json");
    return {name, version};
  }
}
