import {IWizzairPrice} from "./price";

export interface IWizzairFlightRoute {
  airportFrom: string;
  airportTo: string;
  operator: "WIZZAIR";
}

export interface IWizzairFlight {
  departureStation: string;
  arrivalStation: string;
  date: Date;
  arrivalDate: Date;
  price: IWizzairPrice;
  priceType: string;
  departureDate: string;
  classOfService: string;
  hasMacFlight: boolean;
}

export interface IWizzairOneWayFare {
  outboundFlights: IWizzairFlight[];
}

export interface IWizzairRoundTripFare extends IWizzairOneWayFare {
  returnFlights: IWizzairFlight[];
}

export interface IWizzairFlightResult<T extends IWizzairOneWayFare> {
  fares: T[];
}
