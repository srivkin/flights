import {HttpModule, Module} from "@nestjs/common";
import {AppController} from "./app.controller";
import {FlightsModule} from "./flights/flights.module";
import {HTTP} from "./flights/scrappers/http.config";

@Module({
  imports: [
    HttpModule.register({
      ...HTTP
    }),
    FlightsModule
  ],
  controllers: [AppController],
})
export class AppModule {}
