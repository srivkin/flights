import {Observable} from "rxjs";
import {AirportLocation, Flight, RoundTripFlight} from "../dtos/responses";
import {OneWaySearchDto} from "../dtos/requests";
import {Route} from "../dtos/responses/route.dto";

export interface ScrapperApi {
  readonly company: string;

  airports(): Observable<AirportLocation[]>;
  routes(): Observable<Route[]>;
  oneWay(request: OneWaySearchDto): Observable<Flight[]>;
  roundTrip(request: OneWaySearchDto): Observable<RoundTripFlight[]>;
}
