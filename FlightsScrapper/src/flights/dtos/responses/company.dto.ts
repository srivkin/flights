import {AirportLocation} from "./airport.dto";
import {ApiModelProperty} from "@nestjs/swagger";

export class Company {
  @ApiModelProperty()
  public readonly name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export class CompanyAirports {
  @ApiModelProperty({type: Company})
  public readonly company: Company;

  @ApiModelProperty({type: AirportLocation, isArray: true})
  public readonly airports: AirportLocation[];

  constructor(company: Company, airports: AirportLocation[]) {
    this.company = company;
    this.airports = airports;
  }
}
