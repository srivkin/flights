import {Logger} from "@nestjs/common";
import {stringifier} from "../utils";

export function LogInputArgs() {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    // save a reference to the original method this way we keep the values currently in the
    // descriptor and don't overwrite what another decorator might have done to the descriptor.
    if (descriptor === undefined) {
      descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
    }

    const originalMethod = descriptor.value;

    descriptor.value = function() {
      const args = [];
      for (let _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }
      const handler = `${target.constructor.name}\\${propertyKey}`;
      Logger.log(`Arguments ${stringifier(5)(args)}`, handler, false);
      return originalMethod.apply(this, args);
    };

    return descriptor;
  };
}
