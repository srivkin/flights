import {ApiModelProperty} from "@nestjs/swagger";
import {Transform} from "class-transformer";
import {IsDate, IsNotEmpty, IsString, MaxLength, MinDate, MinLength, IsEnum} from "class-validator";

import {DATE_FORMAT, toDate, TODAY} from "../../../core";

export enum TimeUnit {
  Day = "day",
  Week = "week",
  Month = "month",
  Year = "year"
}

export class OneWaySearchDto {
  @ApiModelProperty({
    type: String,
    minimum: 3,
    maximum: 3,
    required: true,
    description: "Iata code of an airport"
  })
  @Transform((v: string) => v && v.toUpperCase())
  @IsString()
  @IsNotEmpty({message: "Must specify from destination"})
  @MinLength(3, {message: "iata code must be at least 3 characters"})
  @MaxLength(3, {message: "iata code must be at most 3 characters"})
  public fromAirport = "";

  @ApiModelProperty({
    type: Date,
    required: false,
    default: TODAY.format(DATE_FORMAT),
    description: `Departure date, defaults to today. Use following format ${DATE_FORMAT} or numeric equivalent`
  })
  @Transform(toDate())
  @IsDate()
  @MinDate(TODAY.toDate())
  public departureDate = TODAY.toDate();

  @ApiModelProperty({
    type: TimeUnit,
    enum: Object.values(TimeUnit),
    default: TimeUnit.Day,
    required: false
  })
  @IsEnum(TimeUnit)
  public forecast = TimeUnit.Day;
}
