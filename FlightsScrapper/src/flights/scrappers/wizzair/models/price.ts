export interface IWizzairPrice {
  amount: number;
  currencyCode: string;
}
