export {stringifier} from "./stringifier";
export {compare, ascendingNumeric, ascendingString} from "./comparison";
export * from "./di";
export * from "./date";

export const CURRENCY = "EUR";

export function isEmptyObject(o: any): boolean {
  return typeof o === "object" && Object.keys(o).length === 0;
}

export function parseBoolean(v: any): boolean | undefined {
  if (v) {
    if (v === "true") {
      return true;
    } else if (v === "false") {
      return false;
    }
  }
  return undefined;
}
