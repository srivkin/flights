import {stringify as queryStringify} from "querystring";

import * as moment from "moment";

import {AirportDefBuilder, FlightBuilder, Flight, RoundTripFlight} from "../../dtos/responses";
import {OneWaySearchDto, TwoWaySearchDto} from "../../dtos/requests";

import {IWizzairFlight, IWizzairRoundTripFare} from "./models";
import {CURRENCY} from "../../../core";

export class WizzairUtil {
  public static readonly RA_NAME = "Wizzair";
  private static readonly RA_DATE_FORMAT = "YYYY-MM-DD";

  public oneWayQuery(request: OneWaySearchDto): string {
    return queryStringify(this.composeOneWayQuery(request));
  }

  public roundTripQuery(request: TwoWaySearchDto): string {
    const back = moment(request.departureDate).add(request.returnIn, request.returnInTimeUnit);
    const twoWay = {
      inboundDepartureDateFrom: back.format(WizzairUtil.RA_DATE_FORMAT),
      inboundDepartureDateTo: back.format(WizzairUtil.RA_DATE_FORMAT)
    };
    return queryStringify({...this.composeOneWayQuery(request), ...twoWay});
  }

  public toRoundTrip(rtf: IWizzairRoundTripFare): RoundTripFlight[] {
    const roundTripFlight: RoundTripFlight[] = [];
    const outboundFlights: Flight[] = rtf.outboundFlights.filter(f => f.price).map(of => this.toFlight(of));
    const returnFlights: Flight[] = rtf.returnFlights.filter(f => f.price).map(of => this.toFlight(of));

    let i = 0;
    for (i; i < outboundFlights.length; i++) {
      const tripFlight: RoundTripFlight = new RoundTripFlight(
        outboundFlights[i],
        returnFlights[i],
        outboundFlights[i].price + (returnFlights[i] ? returnFlights[i].price : 0)
      );
      roundTripFlight.push(tripFlight);
    }
    return roundTripFlight;
  }

  public toFlight(f: IWizzairFlight): Flight {
    return FlightBuilder.for(WizzairUtil.RA_NAME)
      .flightWillCost(f.price.amount)
      .source(AirportDefBuilder.withCode(f.departureStation).description(f.departureStation, f.arrivalStation))
      .departure(new Date(f.date || f.departureDate))
      .destination(AirportDefBuilder.withCode(f.arrivalStation).description(f.arrivalStation, f.arrivalStation))
      .arrives(new Date(f.arrivalDate))
      .build();
  }

  private composeOneWayQuery(request: OneWaySearchDto): {} {
    const md = moment(request.departureDate);
    const oneWay = {
      departureAirportIataCode: request.fromAirport,
      outboundDepartureDateFrom: md.format(WizzairUtil.RA_DATE_FORMAT),
      outboundDepartureDateTo: md.add(1, request.forecast).format(WizzairUtil.RA_DATE_FORMAT),
      currency: CURRENCY
    };
    return oneWay;
  }
}
