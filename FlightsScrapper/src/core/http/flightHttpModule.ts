import {Module} from "@nestjs/common";
import {FlightHttpService} from "./flight-http.service";

export interface IConfig {
  baseURL: string;
}
@Module({
  providers: [
    FlightHttpService,
  ],
  exports: [FlightHttpService],
})
export class FlightHttpModule {
  public static register(config?: IConfig) {
    if (config && config.baseURL) {
      FlightHttpService.prototype.setBaseUrl(config.baseURL);
    }
    return {
      module: FlightHttpModule
    };
  }
}