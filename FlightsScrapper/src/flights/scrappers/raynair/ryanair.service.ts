import {HttpService, Injectable} from "@nestjs/common";

import {Observable, empty} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {AirportLocation, Coordinate, Flight, RoundTripFlight} from "../../dtos/responses";
import {OneWaySearchDto, TwoWaySearchDto} from "../../dtos/requests";

import {ScrapperApi} from "../scrapper.api";

import {
  IRyanairAirport,
  IRyanairFlightResult,
  IRyanairOneWayFare,
  IRyanairRoundTripFare
} from "./models";
import {RyanairUtil} from "./ryanair.util";
import {Route} from "../../dtos/responses/route.dto";

type RyanairDate = "yyyy-MM-dd";
interface Fare {
  departureAirportIataCode: string;
  arrivalAirportIataCode?: string;
  outboundDepartureDateFrom: RyanairDate;
  outboundDepartureDateTo: RyanairDate;
  limit?: number;
  currency?: "USD";
  priceValueTo?: number;
}
// tslint:disable-next-line:interface-over-type-literal
export interface RyanairAPI {
  "farefinder/3/oneWayFares": Fare;
  "farefinder/3/roundTripFares": Fare & {
    inboundDepartureDateFrom: RyanairDate;
    inboundDepartureDateTo: RyanairDate;
  };
  "core/3/routes": null;
  "core/3/routes/{airportFrom}": null;
  "core/3/routes/{airportFrom}/iataCodes": null;
  "timetable/3/schedules/{departure}/{arrival}/years/{year}/months/{month}": null;
  "timetable/3/schedules/{departure}/{arrival}/availability": null;
}

const RA_V = 3;
@Injectable()
export class RyanairService implements ScrapperApi {
  private _util = new RyanairUtil();

  get company() {
    return RyanairUtil.RA_NAME;
  }

  constructor(private _ryanairHttp: HttpService) {}

  public airports(): Observable<AirportLocation[]> {
    return this._ryanairHttp
      .get<IRyanairAirport[]>(`core/${RA_V}/airports`)
      .pipe(
        map(({data}) =>
          data.map(
            airport =>
              new AirportLocation(
                airport.iataCode,
                airport.name,
                Coordinate.from(airport.coordinates.longitude, airport.coordinates.latitude)
              )
          )
        )
      );
  }

  public oneWay(request: OneWaySearchDto): Observable<Flight[]> {
    const url = `farefinder/${RA_V}/oneWayFares?${this._util.oneWayQuery(request)}`;

    return this._ryanairHttp.get<IRyanairFlightResult<IRyanairOneWayFare>>(url).pipe(
      map(({data}) => data.fares.map(f => this._util.toFlight(f.outbound))),
      catchError(() => empty())
    );
  }

  public roundTrip(request: TwoWaySearchDto): Observable<RoundTripFlight[]> {
    const url = `farefinder/${RA_V}/roundTripFares?${this._util.roundTripQuery(request)}`;

    return this._ryanairHttp.get<IRyanairFlightResult<IRyanairRoundTripFare>>(url).pipe(
      map(({data}) => data.fares.map(f => this._util.toRoundTrip(f))),
      catchError(() => empty())
    );
  }

  public routes(): Observable<Route[]> {
    return null;
  }
}
