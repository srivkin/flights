export interface IRyanairAirportDef {
  iataCode: string;
  name: string;
  seoName: string;
}
export interface IRyanairAirport extends IRyanairAirportDef {
  coordinates: {
    latitude: number;
    longitude: number;
  };
  base: boolean;
  regionCode: string;
  cityCode: string;
  currencyCode: string;
  routes: string[];
  seasonalRoutes: string[];
  categories: string[];
  priority: number;
}
