import {Test, TestingModule} from "@nestjs/testing";

import {moduleMetadataFrom} from "../core";

import {FlightsService} from "./flights.service";
import {FlightsModule} from "./flights.module";

describe("FlightsService", () => {
  let service: FlightsService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule(
      moduleMetadataFrom(FlightsModule)
    ).compile();
    service = module.get<FlightsService>(FlightsService);
  });
  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
