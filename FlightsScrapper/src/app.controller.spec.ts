import {Test, TestingModule} from "@nestjs/testing";
import {AppController} from "./app.controller";
import {AppModule} from "./app.module";
import {moduleMetadataFrom} from "./core";

describe("AppController", () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule(moduleMetadataFrom(AppModule)).compile();
  });

  describe("root", () => {
    it("should be defined", () => {
      const controller = app.get<AppController>(AppController);
      expect(controller).toBeDefined();
    });
    it("should return name and version", () => {
      const appController = app.get<AppController>(AppController);
      const {name, version} = require("../package.json");
      expect(appController.root()).toEqual({name, version});
    });
  });
});
