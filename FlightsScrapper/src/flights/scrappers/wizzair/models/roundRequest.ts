export interface FlightList {
  departureStation: string;
  arrivalStation: string;
  from: string;
  to: string;
}

export interface WizzairRoundTripRequest {
  flightList: FlightList[];
  priceType: string;
  adultCount: number;
  childCount: number;
  infantCount: number;
}