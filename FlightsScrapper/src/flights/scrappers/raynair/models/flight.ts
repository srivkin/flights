import {IRyanairAirportDef} from "./airport";
import {IRyanairPrice} from "./price";

export interface IRyanairFlightRoute {
  airportFrom: string;
  airportTo: string;
  operator: "RYANAIR";
}

export interface IRyanairFlight {
  departureAirport: {countryName: string} & IRyanairAirportDef;
  arrivalAirport: {countryName: string} & IRyanairAirportDef;
  departureDate: Date;
  arrivalDate: Date;
  price: IRyanairPrice;
}

export interface IRyanairOneWayFare {
  outbound: IRyanairFlight;
  summary: {
    price: IRyanairPrice;
  };
}

export interface IRyanairRoundTripFare extends IRyanairOneWayFare {
  inbound: IRyanairFlight;
}

export interface IRyanairFlightResult<T extends IRyanairOneWayFare> {
  fares: T[];
}
