import {ApiModelProperty} from "@nestjs/swagger";
import {IsBoolean, IsEnum, IsIn, IsNumber, IsPositive, ValidateIf, Max, Min} from "class-validator";
import {Transform, Type} from "class-transformer";

import {parseBoolean} from "../../../core";
import {OneWaySearchDto, TimeUnit} from "./oneWay-search.dto";

export class TwoWaySearchDto extends OneWaySearchDto {
  @ApiModelProperty({
    type: Boolean,
    required: false,
    default: false,
    description: "Setting it to true will try to foresee into the future"
  })
  @Transform(v => parseBoolean(v) || false)
  @IsBoolean()
  public predict = false;

  @ApiModelProperty({
    type: Number,
    required: false,
    default: 1,
    description: "Will try to scan up different return dates from return date + scanRange"
  })
  @Type(() => Number)
  @ValidateIf((o: TwoWaySearchDto) => o.predict)
  @IsNumber({allowNaN: false, allowInfinity: false})
  @IsPositive()
  @Max(15)
  public scanRange = 1;

  @ApiModelProperty({
    type: Number,
    required: false,
    default: 7,
    description: "Will reduce flight which their overall days exceed limitDays"
  })
  @Type(() => Number)
  @ValidateIf((o: TwoWaySearchDto) => o.predict)
  @IsNumber({allowNaN: false, allowInfinity: false})
  @IsPositive()
  @Min(2)
  public limitDays = 7;

  @ApiModelProperty({
    type: Number,
    required: false,
    default: 1
  })
  @Type(() => Number)
  @IsNumber({allowNaN: false, allowInfinity: false})
  @IsPositive()
  public returnIn = 1;

  @ApiModelProperty({
    type: TimeUnit,
    required: false,
    default: TimeUnit.Day,
    enum: [TimeUnit.Day, TimeUnit.Week]
  })
  @IsEnum(TimeUnit)
  @IsIn([TimeUnit.Day, TimeUnit.Week])
  public returnInTimeUnit = TimeUnit.Day;
}
