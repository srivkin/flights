import {ApiModelProperty} from "@nestjs/swagger";
import {AirportDef, AirportLocation, Coordinate} from "./airport.dto";

export class Route extends AirportLocation {
  @ApiModelProperty({type: String})
  public readonly identifier: string;

  @ApiModelProperty()
  public connections: AirportDef[];

  constructor(identifier: string, name: string, coordinate: Coordinate, connections: AirportDef[]) {
    super(identifier, name, coordinate);
    this.connections = connections;
  }
}
