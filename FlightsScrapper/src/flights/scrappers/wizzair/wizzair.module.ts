import {Module} from "@nestjs/common";

import {WizzairService} from "./wizzair.service";
import {FlightHttpModule} from "../../../core/http/flightHttpModule";

export {WizzairService} from "./wizzair.service";

@Module({
  imports: [
    FlightHttpModule.register()
  ],
  providers: [WizzairService],
  exports: [WizzairService]
})
export class WizzairModule {}
